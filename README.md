# Trace technique démonstration Open Policy Agent (OPA)

## Objectif

Utiliser OPA (GateKeeper) pour restreindre l'accès à certains registres d'images dans un cluster Kubernetes.

## Prérequis

### Cluster

Dans cette démonstration nous allons utiliser ```minikube``` (avec virtualbox) pour mettre en place un cluster Kubernetes d'un seul noeud.

#### Installation de minikube

```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
chmod +x minikube
sudo mkdir -p /usr/local/bin/
sudo install minikube /usr/local/bin/
```

#### Installation de virtualbox

```
sudo apt-get update
sudo apt-get install virtualbox
```

#### Installation de l'outil kubectl

```
curl -LO https://dl.k8s.io/release/$(curl -Ls https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mkdir -p /usr/local/bin/
sudo install kubectl /usr/local/bin/
```

Il faut ensuite démarrer le cluster avec ```minikube start --driver=virtualbox```. (kubectl sera configuré pour utiliser ce cluster)

### GateKeeper

Une fois le cluster démarré, on doit installer le contrôleur d'admission GateKeeper (basé sur OPA).

```
kubectl apply -f https://raw.githubusercontent.com/open-policy-agent/gatekeeper/v3.14.0/deploy/gatekeeper.yaml
```

## Démonstration

Maintenant que tout est en place, il faut ajouter nos politiques de sécurité. Dans cette démontration nous allons faire en sorte que toutes les images provenant du registre d'image ```no0x``` ne soit pas autorisé.

### ConstraintTemplate

Avant de pouvoir définir une contrainte, nous devons d'abord définir un modèle de contrainte (ConstraintTemplate) qui décrit le code Rego et le schéma de la contrainte.

Ici, on aura alors :

```
kubectl apply -f template.yaml
```

ou (si vous n'avez pas cloné le repo) :

```
kubectl apply -f - <<EOF
apiVersion: templates.gatekeeper.sh/v1
kind: ConstraintTemplate
metadata:
  name: k8sdisallowedrepos
  annotations:
    metadata.gatekeeper.sh/title: "Disallowed Repositories"
    metadata.gatekeeper.sh/version: 1.0.0
    description: >-
      Disallowed container repositories that begin with a string from the specified list.
spec:
  crd:
    spec:
      names:
        kind: K8sDisallowedRepos
      validation:
        # Schema for the parameters field
        openAPIV3Schema:
          type: object
          properties:
            repos:
              description: The list of prefixes a container image is not allowed to have.
              type: array
              items:
                type: string
  targets:
    - target: admission.k8s.gatekeeper.sh
      rego: |
        package k8sdisallowedrepos

        violation[{"msg": msg}] {
          container := input.review.object.spec.containers[_]
          image := container.image
          startswith(image, input.parameters.repos[_])
          msg := sprintf("container <%v> has an invalid image repo <%v>, disallowed repos are %v", [container.name, container.image, input.parameters.repos])
        }

        violation[{"msg": msg}] {
          container := input.review.object.spec.initContainers[_]
          image := container.image
          startswith(image, input.parameters.repos[_])
          msg := sprintf("initContainer <%v> has an invalid image repo <%v>, disallowed repos are %v", [container.name, container.image, input.parameters.repos])
        }

        violation[{"msg": msg}] {
          container := input.review.object.spec.ephemeralContainers[_]
          image := container.image
          startswith(image, input.parameters.repos[_])
          msg := sprintf("ephemeralContainer <%v> has an invalid image repo <%v>, disallowed repos are %v", [container.name, container.image, input.parameters.repos])
        }
EOF
```

### Constraint

Nous allons ensuite créer notre contrainte à partir du modèle précédant, en précisant les éléments sur lesquels elle s'applique, et (dans cet exemple) la liste des registres d'images à interdire :

```
kubectl apply -f constraint.yaml
```

ou (si vous n'avez pas cloné le repo) :

```
kubectl apply -f - <<EOF
apiVersion: constraints.gatekeeper.sh/v1beta1
kind: K8sDisallowedRepos
metadata:
  name: repo-must-not-be-no0x
spec:
  match:
    kinds:
      - apiGroups: [""]
        kinds: ["Pod"]
  parameters:
    repos:
      - "no0x/"
EOF
```

### Vérification

Pour vérifier que notre contrainte est bien active on va essayer de créer 2 pods, un avec l'image ```nginx``` et un avec l'image ```no0x/influxdb```.

Créons le premier pod avec l'image ```nginx``` :

```
kubectl apply -f example-allowed.yaml
```

ou (si vous n'avez pas cloné le repo) :

```
kubectl apply -f - <<EOF
apiVersion: v1
kind: Pod
metadata:
  name: example-allowed
spec:
  containers:
  - image: nginx
    name: example-allowed
EOF
````

Ici, l'image utilsée ne venant pas du registre qu'on a précisé dans la contrainte, on obtient ce retour :

```
pod/example-allowed created
```

Essayons maintenant de créer l'autre pod avec l'image ```no0x/influxdb``` :

```
kubectl apply -f example-disallowed.yaml
```

ou (si vous n'avez pas cloné le repo) :

```
kubectl apply -f - <<EOF
apiVersion: v1
kind: Pod
metadata:
  name: example-disallowed
spec:
  containers:
  - image: no0x/influxdb
    name: example-disallowed
EOF
```
On obtient ce retour : 

```
Error from server (Forbidden): error when applying patch:
{"metadata":{"annotations":{"kubectl.kubernetes.io/last-applied-configuration":"{\"apiVersion\":\"v1\",\"kind\":\"Pod\",\"metadata\":{\"annotations\":{},\"name\":\"example-allowed\",\"namespace\":\"default\"},\"spec\":{\"containers\":[{\"image\":\"no0x/influxdb\",\"name\":\"example-allowed\"}]}}\n"}},"spec":{"$setElementOrder/containers":[{"name":"example-allowed"}],"containers":[{"image":"no0x/influxdb","name":"example-allowed"}]}}
to:
Resource: "/v1, Resource=pods", GroupVersionKind: "/v1, Kind=Pod"
Name: "example-allowed", Namespace: "default"
for: "STDIN": error when patching "STDIN": admission webhook "validation.gatekeeper.sh" denied the request: [repo-must-not-be-k8s-gcr-io] container <example-allowed> has an invalid image repo <no0x/influxdb>, disallowed repos are ["no0x/"]
```

Notre contrainte a bien été respectée.
